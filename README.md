# Projet 
Projet tutoré LP PRISM Orsay 2017-2018

## SQLite

### MCD :

![enter image description here](https://gitlab.com/tinyman/prism-network/raw/53f121d2a608657d2c8ef091ccfc5e85a38b07c4/scripts/sqlite/mioga.PNG)

**Pour parcourir la bd** on vous conseille DB Browser for SQLite : http://sqlitebrowser.org/

**Pour installer SQLlite dans le proj :**

`$ npm install sqlite3`

**Les password sont tous "12345" hashé en SHA1**

**Voici un exemple d'utilisation :** (pas testé)

    var sqlite3 = require('sqlite3').verbose();
    var db = new sqlite3.Database('./scripts/sqlite/mioga'); // à confirmer
    
    var express = require('express');
    var app = express();
    
    // Exemple de select all
    app.get("/users", function(req, res, next) {
    	db.all('SELECT * FROM utilisateur;', function(err, data) {
    		res.json(data);
    		if(err){
    			console.error(err);
    			throw err;
    		}
    	});
    });
    
    // Exemple d'insert
    app.post("/insertMessage", function(req, res, next) {
    	// On récupère les variables de la requête
    	var utilisateur = req.body.utilisateur;
    	var contenu = req.body.contenu;
    	db.run("INSERT INTO message (utilisateur,contenue,dateCreation) VALUES (?,?,new Date());",[utilisateur,contenu], function (err, result) {
    		if (err) throw err;		
    		console.log("1 record inserted");
    	});
    });
        
