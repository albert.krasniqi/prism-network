BEGIN TRANSACTION;
DROP TABLE IF EXISTS `utilisateur_groupe`;
CREATE TABLE IF NOT EXISTS `utilisateur_groupe` (
	`utilisateur`	INTEGER NOT NULL,
	`groupe`	INTEGER NOT NULL,
	FOREIGN KEY(`groupe`) REFERENCES `groupe`(`id`),
	FOREIGN KEY(`utilisateur`) REFERENCES `utilisateur`(`id`),
	PRIMARY KEY(`utilisateur`,`groupe`)
);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (1,1);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (1,2);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (1,5);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (2,1);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (2,7);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (3,1);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (3,4);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (4,1);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (4,2);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (5,5);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (6,2);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (6,4);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (6,5);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (7,5);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (8,2);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (9,7);
INSERT INTO `utilisateur_groupe` (utilisateur,groupe) VALUES (10,7);
DROP TABLE IF EXISTS `utilisateur`;
CREATE TABLE IF NOT EXISTS `utilisateur` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`mail`	TEXT UNIQUE,
	`password`	TEXT,
	`nom`	TEXT,
	`prenom`	TEXT
);
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (1,'utilisateur1@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur1','PrenomUtilisateur1');
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (2,'utilisateur2@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur2','PrenomUtilisateur2');
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (3,'utilisateur3@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur3','PrenomUtilisateur3');
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (4,'utilisateur4@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur4','PrenomUtilisateur4');
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (5,'utilisateur5@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur5','PrenomUtilisateur5');
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (6,'utilisateur6@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur6','PrenomUtilisateur6');
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (7,'utilisateur7@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur7','PrenomUtilisateur7');
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (8,'utilisateur8@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur8','PrenomUtilisateur8');
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (9,'utilisateur9@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur9','PrenomUtilisateur9');
INSERT INTO `utilisateur` (id,mail,password,nom,prenom) VALUES (10,'utilisateur10@gmail.com','8cb2237d0679ca88db6464eac60da96345513964','NomUtilisateur10','PrenomUtilisateur10');
DROP TABLE IF EXISTS `message_groupe`;
CREATE TABLE IF NOT EXISTS `message_groupe` (
	`message`	INTEGER NOT NULL,
	`groupe`	INTEGER NOT NULL,
	`visible`	INTEGER DEFAULT 1,
	`plus`	INTEGER,
	`moins`	INTEGER,
	FOREIGN KEY(`groupe`) REFERENCES `groupe`(`id`),
	PRIMARY KEY(`message`,`groupe`),
	FOREIGN KEY(`message`) REFERENCES `message`(`id`)
);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (1,1,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (1,2,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (2,7,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (2,1,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (3,4,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (3,1,0,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (4,1,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (6,2,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (6,5,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (7,5,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (8,2,0,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (9,7,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (10,7,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (11,5,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (11,2,0,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (12,7,0,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (12,1,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (13,3,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (13,1,0,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (14,2,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (14,1,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (16,4,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (16,2,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (16,5,0,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (17,5,0,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (18,2,0,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (19,7,0,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (20,7,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (5,5,1,0,0);
INSERT INTO `message_groupe` (message,groupe,visible,plus,moins) VALUES (15,5,1,0,0);
DROP TABLE IF EXISTS `message`;
CREATE TABLE IF NOT EXISTS `message` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`utilisateur`	INTEGER,
	`contenu`	TEXT,
	`dateCreation`	TEXT,
	`dateModif`	TEXT
);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (1,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (2,2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:21:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (3,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (4,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (5,5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (6,6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (7,7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (8,8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (9,9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (10,10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (11,1,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (12,2,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (13,3,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (14,4,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (15,5,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (16,6,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (17,7,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (18,8,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (19,9,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
INSERT INTO `message` (id,utilisateur,contenu,dateCreation,dateModif) VALUES (20,10,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2018-02-14 10:20:05',NULL);
DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
	`id`	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	`nom`	TEXT,
	`admin`	INTEGER,
	FOREIGN KEY(`admin`) REFERENCES `utilisateur`(`id`)
);
INSERT INTO `groupe` (id,nom,admin) VALUES (1,'Projet Mioga',1);
INSERT INTO `groupe` (id,nom,admin) VALUES (2,'Groupe des chefs',1);
INSERT INTO `groupe` (id,nom,admin) VALUES (3,'Techs',NULL);
INSERT INTO `groupe` (id,nom,admin) VALUES (4,'Club de Foot ',6);
INSERT INTO `groupe` (id,nom,admin) VALUES (5,'HelpDesk',7);
INSERT INTO `groupe` (id,nom,admin) VALUES (6,'Dev Interne',NULL);
INSERT INTO `groupe` (id,nom,admin) VALUES (7,'Dev Externe',2);
INSERT INTO `groupe` (id,nom,admin) VALUES (8,'Bénévoles de la Salle de Sport',NULL);
INSERT INTO `groupe` (id,nom,admin) VALUES (9,'Projet Portail',NULL);
INSERT INTO `groupe` (id,nom,admin) VALUES (10,'Recherche et Developpement',NULL);
DROP TABLE IF EXISTS `favori`;
CREATE TABLE IF NOT EXISTS `favori` (
	`message`	INTEGER NOT NULL,
	`utilisateur`	INTEGER NOT NULL,
	FOREIGN KEY(`utilisateur`) REFERENCES `utilisateur`(`id`),
	PRIMARY KEY(`message`,`utilisateur`)
);
COMMIT;
