import { State } from '@/store/state';
import { GetterTree } from 'vuex';

export const user = 'USER';
export const groups = 'GROUPS';
export const currentGroup = 'CURRENT_GROUP';
export const currentGroupId = 'CURRENT_GROUP_ID';
export const pending = 'PENDING';
export const isLoggedIn = 'CURRENT_GROUP_ID';



export const getters: GetterTree<State, State> = {
  [user]: (state: State) => state.user,
  [groups]: (state: State) => state.groups,
  [currentGroup]: (state: State) => state.currentGroup,
  [currentGroupId]: (state: State, g) => g[currentGroup] && g[currentGroup].id,
};
