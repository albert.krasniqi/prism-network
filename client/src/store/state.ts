import { User } from '@/models/User';
import { Message } from '@/models/Message';
import { Group } from '@/models/Group';

export interface State {
  user: User | null;
  isLoggedIn: boolean;
  pending: boolean;
  messages: Map<string, Message>;
  groups: Group[];
  currentGroup: Group | null;
}

const initialState: State = {
  user: null,
  isLoggedIn: false,
  pending: false,
  messages: new Map<string, Message>(),
  groups: [],
  currentGroup: null,
};

export default initialState;
