import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
// @ts-ignore: no-implicit-any
import VueAutosize from 'vue-autosize';
// @ts-ignore: no-implicit-any
import VuePrism from 'vue-prism';
import './styles';

Vue.config.productionTip = false;

Vue.use(VueAutosize);
Vue.use(VuePrism);

new Vue({
  router,
  store,
  render: (h) => h(App),
}).$mount('#app');
