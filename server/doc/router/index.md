# The router

## Existing routes

- [User](user.md)

## Using the router

There is nothing more to be done tu start using the router. Just point your
browser to a declared route.

## Defining your routes

To define your routes to, for instance, a "`misc`" section, you will need to
create a file named "`src/routes/misc.js`" with the following structure:

```javascript
'use strict';

import Mioga from '../core/Mioga';
import User from '../objects/User';

module.exports = {
	// This defines the "/misc/" section
	'/': {
		// This will handle "GET /misc/"
		get: function(req, res, next) {
		},

		// This will handle "POST /misc/"
		post: function(req, res, next) {
		}
	},

	// This defines the "/misc/<id>" section
	'/:id': {
		// This will handle "GET /misc/<id>"
		get: function(req, res, next) {
		},

		// This will handle "PUT /misc/<id>"
		put: function(req, res, next) {
		},

		// This will handle "DELETE /misc/<id>"
		delete: function(req, res, next) {
		}
	}
}
```

The contents of the handlers is described in the "[Basic
routing](http://expressjs.com/en/starter/basic-routing.html)" and
"[Routing](http://expressjs.com/en/guide/routing.html)" pages of ExpressJS
documentation.
