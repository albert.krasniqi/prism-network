# User routes

The following routes can handle users.

## `GET "/user/"`

Description: get all existing users.

Arguments: none.

Return value: all users.

## `POST "/user/"`

Description: create a user.

Arguments: user attributes as JSON in request body:

```javascript
{
	"firstname": "Firstname",
	"lastname": "Lastname",
	"email": "E-mail address"
}
```

Return value: the created user attributes.

## `GET "/user/<id>"`

Description: get a user by ID.

Arguments: none.

Return value: the matching user attributes.

## `PUT "/user/<id>"`

Description: update a user by ID.

Arguments: user attributes as JSON in request body:

```javascript
{
	"firstname": "Firstname",
	"lastname": "Lastname",
	"email": "E-mail address"
}
```

Return value: the updated user attributes.

## `DELETE "/user/<id>"`

Description: delete a user by ID.

Arguments: none.

Return value: the request execution status.
