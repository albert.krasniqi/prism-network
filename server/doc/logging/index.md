# The logging system

Mioga provides a multi-level logging system. To use it into a module named "`Foo`", the following code should be used:

```
import Logger from './path/to/Logger';

var log = new Logger(mioga, 'Foo');
```

The different levels are available:

- TRACE
- DEBUG
- INFO
- WARN
- ERROR
- SILENT

Default level is "`ERROR`".

## Logging level configuration

To change the logging level for module "`Foo`", just add the following to "`config/user.json`":

```
"log": {
	"Foo": "DEBUG"
}
```
