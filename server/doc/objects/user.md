# User

This class can handle a single user or a list of users.

This page shows how to use it.

- load module:

```
import User from './objects/User';
```

- create a new user:

```
var u1 = new User(mioga, {
	firstname: "John",
	lastname: "Doe",
	email: "jdoe@noname.org"
});
u1.store();
```

- load a user:

```
var u1 = new User(mioga, {
	firstname: "John",
	lastname: "Doe",
	email: "jdoe@noname.org"
});
u1.load();
```

- update a user:

```
var u1 = new User(mioga, {
	firstname: "John",
	lastname: "Doe",
	email: "jdoe@noname.org"
});
u1.load()
	.then(response => {
		u1.email = 'john@doe.org';
		u1.store();
	});
```

- load all users:

```
var u1 = new User(mioga);
u1.load()
```

- dump user attributes or user list attributes:

```
console.dir(u1.get() ;
```
