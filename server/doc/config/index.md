# The Configuration System

## How it works

The configuration system loads its data from the "`config/`" directory.
It will fetch its contents into an object that provides a getter function.

The directory has the following structure:

```
config
└── defaults
    ├── directory
    │   └── file.json
    └── main.json
```

The "`config/`" directory contains a "`defaults/`" subdirectory.
**No user override should be inserted into this directory.** It is only meant to contain default upstream configuration values.

User overrides should be added to file « `config/user.json` » and will overload defaults.

## Configuration Keys Reference

- `api.port` (`3000`)

	This sets the port number the server will listen on.

- `client.path` (`doc`)

	Path to directory containing client code.
	For convenience, client code is outside this repository, default client pages are HTML conversion of help pages.

- `db.type` (`sqlite3`)

	Database driver to be used.

- `db.mongodb.url` (`mongodb://localhost/mioga3`)

	MongoDB database URL.

- `db.sqlite3.path` (`data/db.sqlite3`)

	SQLite3 database file path.

- `log.format` (`dev`)

	The format used by the logging engine to write access logs.
	See [Morgan](https://github.com/expressjs/morgan#predefined-formats) documentation for details.

- `log.level.Server` (`INFO`)

	The startup script log level.
