# Mioga3 prototype based on ExpressJS

## Startup

- install dependencies:

```
npm install
```

- startup development server:

```
npm run dev
```

- watch at logs:

```
npm run logs
```

- point your Web browser to `http://localhost:3000`
