/************************************************************
 *
 *    This is the main Mioga startup script.
 *
 ************************************************************/

'use strict';

import Mioga from './core/Mioga';
import Logger from './core/Logger';
import Router from './core/Router';


/**
 * Load Mioga-specific components
 */
var mioga = new Mioga();
var log = new Logger(mioga, 'Server');

log.info('Mioga3 is starting up');


/**
 * Load ExpressJS components
 */
var express = require('express');
var http = require('http');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var compression = require('compression');
var helmet = require('helmet');
var methodOverride = require('method-override');
var paginate = require('express-paginate');
var cors = require('cors');
var expressValidator = require('express-validator');
var morgan = require('morgan')


/**
 * Initialize ExpressJS app
 */
var app = express();
var server = http.Server(app);


/**
 * Configure middleware
 */
app.use(cors());
app.use(compression());
app.use(helmet());
app.use(methodOverride('X-HTTP-Method-Override'));
app.use(bodyParser.text());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(expressValidator());
app.use(cookieParser());
app.use(paginate.middleware(10, 100));
app.use(morgan(mioga.get('log.format')));


/**
 * Configure client tree access
 */
app.use(require('express-markdown')({
	directory: mioga.get('client.path')
}));
app.use(express.static(mioga.get('client.path')));


/**
 * Load routes
 */
var router = new Router(mioga, app);


/**
 * Start server
 */
var port = mioga.get('api.port');
server.listen(port, () => {
		log.info('Welcome Mioga3!');
		log.info(`Server is listening http:\/\/localhost:${port}`);
	});
