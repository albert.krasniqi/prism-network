/************************************************************
 *
 * User.sqlite3
 *
 *    User-specific SQLite3 bindings.
 *
 ************************************************************/

'use strict';

const Sequelize = require('sequelize');

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');

		this._model = this._db.define('message', {
			id: Sequelize.UUIDV1,
			contenu: Sequelize.STRING,
			dateCreation: Sequelize.STRING,
			dateModif: Sequelize.STRING
		});

		message.hasOne(utilisateur, { foreignKey: 'utilisateur' })
		message.belongsToMany(groupe, { through: message_groupe })		
		// Automatically create table
		this._model.sync();

		this.log.trace('[schema] Leaving');
	}
};
