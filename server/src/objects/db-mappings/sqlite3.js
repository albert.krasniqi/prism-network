/************************************************************
 *
 * SQLite3 common bindings
 *
 *     These methods are available to all objects when using
 *     SQLite3
 *
 ************************************************************/

'use strict';

const Sequelize = require('sequelize');

module.exports = {
	load: function() {
		this.log.trace('[load] Entering');
		return this._model.findAll({where: this._attr})
			.then(found => {
				if(found.length === 1) {
					// Query returned single record
					this.log.trace(`[load] Single record found in DB, id ${found[0].id}`)
					this._attr = found[0].get({plain: true});
				}
				else if(found.length > 1) {
					// Query returned multiple records
					this._list = [];
					found.forEach(record => {
						this._list.push(record.get({plain: true}));
					});
				}
				else {
					this._attr = {};
					this._list = [];
				}
				this.log.trace('[load] Leaving');
			});
	},

	store: function() {
		this.log.trace('[store] Entering');

		if(!this._attr.id) {
			// Create record
			this.log.trace('[store] Creating new record');
			return this._model.create(this._attr)
					.then(() => {
						this.log.trace('[store] Record created successfully');
						this.log.trace('[store] Leaving');
					})
					.catch(error => {
						this.log.error('[store] Record creation failed');
						this.log.error(error);
						this.log.trace('[store] Leaving');
					});
		}
		else {
			// Update record
			this.log.trace(`[store] Updating existing record ${this._attr.id}`);
			return this._model.update(this._attr, {where: {id: this._attr.id}})
					.then(() => {
						this.log.trace('[store] Record updated successfully');
						this.log.trace('[store] Leaving');
					})
					.catch(error => {
						this.log.error('[store] Record update failed');
						this.log.error(error);
						this.log.trace('[store] Leaving');
					});
		}
	},

	delete: function() {
		this.log.trace('[delete] Entering');

		return this._model.destroy({where: this._attr});

		this.log.trace('[delete] Leaving');
	}
};
