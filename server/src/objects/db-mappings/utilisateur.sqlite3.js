/************************************************************
 *
 * User.sqlite3
 *
 *    User-specific SQLite3 bindings.
 *
 ************************************************************/

'use strict';

const Sequelize = require('sequelize');

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');

		this._model = this._db.define('utilisateur', {
			id: Sequelize.UUIDV1,
			mail: Sequelize.STRING,
			password: Sequelize.STRING,
			nom: Sequelize.STRING,
			prenom: Sequelize.STRING
		});

		utilisateur.belongsToMany(groupe, { through: utilisateur_groupe })
		utilisateur.belongsToMany(message, { through: favori })		

		// Automatically create table
		this._model.sync();

		this.log.trace('[schema] Leaving');
	}
};
