/************************************************************
 *
 * User.mongodb
 *
 *    User-specific MongoDB bindings.
 *
 ************************************************************/

'use strict';

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');

		var schema = this._db.Schema({
			firstname: String,
			lastname: String,
			email: String
		});

		this._model = this._db.models.user || this._db.model('user', schema);

		this.log.trace('[schema] Leaving');
	}
};
