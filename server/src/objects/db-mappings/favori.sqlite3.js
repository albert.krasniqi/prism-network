/************************************************************
 *
 * User.sqlite3
 *
 *    User-specific SQLite3 bindings.
 *
 ************************************************************/

'use strict';

const Sequelize = require('sequelize');

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');
		//Vide car sert juste de table de jointure. 
		//On à déclaré l'association juste du coté utilisateur pour avoir la liste des messages favoris de celui ci.
		//Est-ce qu'on le garde ?
		this._model = this._db.define('favori', {

		});

		// Automatically create table
		this._model.sync();

		this.log.trace('[schema] Leaving');
	}
};
