/************************************************************
 *
 * User.sqlite3
 *
 *    User-specific SQLite3 bindings.
 *
 ************************************************************/

'use strict';

const Sequelize = require('sequelize');

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');
		//Vide car sert juste de table de jointure. 
		//On à déclaré les associations respectivement dans les maps utilisateurs et groupe
		//Est-ce qu'on le garde ?
		this._model = this._db.define('utilisateur_groupe', {

		});

		// Automatically create table
		this._model.sync();

		this.log.trace('[schema] Leaving');
	}
};
