/************************************************************
 *
 * User.sqlite3
 *
 *    User-specific SQLite3 bindings.
 *
 ************************************************************/

'use strict';

const Sequelize = require('sequelize');

module.exports = {
	schema: function() {
		this.log.trace('[schema] Entering');

		this._model = this._db.define('groupe', {
			id: Sequelize.UUIDV1,
			nom: Sequelize.STRING,
		});

		groupe.belongsToMany(utilisateur, { through: utilisateur_groupe })
		groupe.belongsToMany(message, { through: message_groupe })
		groupe.hasOne(utilisateur, { foreignKey: 'admin' })
		

		// Automatically create table
		this._model.sync();

		this.log.trace('[schema] Leaving');
	}
};
