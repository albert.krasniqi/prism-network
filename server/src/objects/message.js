/************************************************************
 *
 * User
 *
 *    This class handles a single user or a list of users.
 *
 ************************************************************/

'use strict';

import BaseObject from './BaseObject';

class message extends BaseObject {
	init(attr){
		this.log.trace("[init] Entering");
		console.log(attr);		
		this.log.trace("[init] Leaving");
	}

	get id() { return this._attr.id };
	set id(val) { this._attr.id = val};
	
	get contenu() { return this._attr.contenu };
	set contenu(val) { this._attr.contenu = val};
	
	get dateCreation() { return this._attr.dateCreation };
	set dateCreation(val) { this._attr.dateCreation = val};
	
	get dateModif() { return this._attr.dateModif };
	set dateModif(val) { this._attr.dateModif = val};

	// utilisateur.findAll({
	//	include: [{
	//	  model: utilisateur_groupe
	//	}]
	//});

}

export default message;
