'use strict';

import Mioga from '../core/Mioga';
import User from '../objects/User';

module.exports = {
	'/': {
		// Get all users
		get: function(req, res, next) {
			var users = new User(req.mioga);
			users.load()
				.then(() => {
					res.send(users.get());
				});
		},

		// Create a user
		post: function(req, res, next) {
			var user = new User(req.mioga, req.body);
			user.store();
			res.send(user.get());
		}
	},

	'/:id': {
		// Get a user
		get: function(req, res, next) {
			var user = new User(req.mioga, {
				id: req.params.id
			});
			user.load()
				.then(() => {
					res.send(user.get());
				});
		},

		// Update a user
		put: function(req, res, next) {
			var user = new User(req.mioga, {
				id: req.params.id
			});
			user.load()
				.then(() => {
					for (var attr in req.body) {
						user[attr] = req.body[attr];
					}
					user.store();
					res.send(user.get());
				});
		},

		// Delete a user
		delete: function(req, res, next) {
			var user = new User(req.mioga, {
				id: req.params.id
			});
			user.delete()
				.then(response => {
					res.send(response);
				});
		}
	}
}
