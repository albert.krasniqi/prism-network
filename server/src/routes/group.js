'use strict';

import Mioga from '../core/Mioga';
import Group from '../objects/Group';

module.exports = {
    '/': {
        //Get all groups
        get: function(req, res, next){
            var groups = new Group(req.mioga);
            groups.load()
                .then(() => {
                    res.send(groups.get());
                })
        },

        // Create a group
		post: function(req, res, next) {
			var group = new Group(req.mioga, req.body);
			group.store();
			res.send(group.get());
		}
    },

    '/:id': {
        //Get a group
        get: function(req, res, next) {
            var group = new Group(req.mioga,{
                id: req.params.id
            });
            group.load()
                .then(() => {
                    res.send(group.get());
                });
        },

    }
}