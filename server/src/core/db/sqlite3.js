'use strict';
import Logger from '../Logger';

export default class SQLite3Driver {
	constructor(mioga) {
		var _this = this;
		this._log = new Logger(mioga, 'SQLite3');
		this._log.trace("Entering");

		var Sequelize = require('sequelize');

		this._path = mioga.get('db.sqlite3.path');

		// Load database
		this._log.debug(`Loading SQLite3 driver (database path: ${_this._path})`);
		this._sequelize = new Sequelize('sqlite:', {
			dialect: 'sqlite',
			storage: _this._path
		});

		// Automatically create tables
		this._sequelize.sync();

		this._log.trace("Leaving");

		return (this._sequelize);
	}
}
